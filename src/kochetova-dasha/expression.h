#include <stdio.h>
#include <iostream>
#include "TStack.h"
#include <cstring>
#include <string>
using namespace std;

int CheckBrackets (char str[])
{
	int ErrorCount=0;    //������� ������
    TStack brackstack (256);
	int BracketTable[128][2];
	int i,j=0;
	cout<<"Open brackets\t"<<"Close brackets\t"<<endl;
	for (i = 0; i <= strlen(str); i++)
	{
		if ((str[i]!='(') && (str[i]!=')'))
			;
		 else if (str[i]=='(')
				brackstack.Put(i);
		   else if (str[i]==')')
		    {
				BracketTable[j][1]=i;
				if (!brackstack.IsEmpty())
				      BracketTable[j][0]=brackstack.Get();
				else 
				{
					BracketTable[j][0]=-1;
				    ErrorCount++;
				}
				cout<<"\t"<<BracketTable[j][0]<<"\t\t"<<BracketTable[j][1]<<"\n"<<endl;
				j++;
		    };
	}

	if (!brackstack.IsEmpty())
	{
		while (!(brackstack.IsEmpty()))
		{
			BracketTable[j][0]=brackstack.Get();
			BracketTable[j][1]=-1;
			ErrorCount++;
			cout<<"\t"<<BracketTable[j][0]<<"\t\t"<<BracketTable[j][1]<<"\n"<<endl;
			j++;
		}
	}
	cout<<ErrorCount<<" errors"<<endl;
	return ErrorCount;
}

int Priority(const char &operation)
{
	switch (operation)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	}
}

bool IsOperation(const char &operation)
{
	if ((operation == '+') || (operation == '-') || (operation == '*') || (operation == '/'))
		return true;
	return false;
}

string ConversionExpression (char str[])
{
	string result;
	if (CheckBrackets(str))
	{
	    cout<<"Error! Add/clean the bracket(s)"<<endl;
		result="error";
	}
	else
	{
	  TStack operation(256);
	  for (int i = 0; i <= strlen(str); i++)
	  {
		  if (isdigit(str[i]))
			  result += str[i];
          if (Priority(str[i]) == 0) 
				operation.Put(str[i]);
		  if (IsOperation(str[i]))
		  {
			   if ((operation.IsEmpty()) || (Priority(str[i]) > Priority((char)operation.GetLast())))
			       operation.Put(str[i]);
			   else 
			   {
				  while (true)
					{
						result += (char)operation.Get();
						if (Priority(str[i]) > Priority((char)operation.GetLast()) || operation.IsEmpty())
							break;
					}
					operation.Put(str[i]);
			   }
			result += ' ';
		  }
	      if (Priority(str[i]) == 1)
			   {
				 while (true)
				 {
					if (operation.IsEmpty())
						break;
					if (Priority((char)operation.GetLast()) == 0)
					{
						operation.Get();
						break;
					}
					result += (char)operation.Get();
				 }
			   }
	  }
	  while (!operation.IsEmpty())
	      result += (char)operation.Get();
	}
	cout<<"The expression in Postfix form\n"<<result<<endl;
	return result;
}

double Calculation(char str[])
{
	string result=ConversionExpression(str);  //������ � ����������� ������
	TStack operand(256);
	char number[256] = "";   //�� ���� ������ �����
	int j=0,i=0;
	double first,second;   //��������������� ���������� ��� ����������
	while (result[i])
	{
		 if (isdigit(result[i]))
		   number[j++] += result[i];
		 else if (*number != 0)
				{
					operand.Put(atof(number));
					memset(number, 0, 256*sizeof(char));
					j = 0;
				}
		if (IsOperation(result[i]))
		{
		 second = operand.Get();
		 first = operand.Get();
		 switch (result[i])
			{
			case '+': operand.Put(first + second); break;
			case '-': operand.Put(first - second); break;
			case '*': operand.Put(first * second); break;
			case '/': operand.Put(first / second); break;
			}
		}
	  i++;
	}
  return operand.Get();
}

