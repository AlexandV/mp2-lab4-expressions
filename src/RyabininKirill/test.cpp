#include "simple.cpp"
#include "gtest.h"

TEST(Transfer, translation_of_integer_expressions)
{
EXPECT_EQ(Transfer("4*(7-5)+4"),"4 7 5-* 4+");
}
TEST(Transfer, translation_of_real_expression)
{
EXPECT_EQ(Transfer("4.4*(7.7-5.5)+4.4"),"4.4 7.7 5.5-* 4.4+");
}
TEST(Transfer, translation_of_little_real_expression)
{
EXPECT_EQ(Transfer("0.054544*(0.0777-0.0555)+0.003445"),"0.054544 0.0777 0.0555-* 0.003445+");
}
TEST(Transfer, translation_of_big_integer_expression)
{
EXPECT_EQ(Transfer("4000000*(7000000-5000000)+4000000"),"4000000 7000000 5000000-* 4000000+");
}
TEST(Computation, computation_of_integer_expression)
{
double res=4*(7-5)+4;
EXPECT_DOUBLE_EQ(Compute("4*(7-5)+4"),result);
}
TEST(Computation, computation_of_real_expression)
{
double res=4.4*(7.7-5.5)+4.4;
EXPECT_DOUBLE_EQ(Compute("4.4*(7.7-5.5)+4.4"),result);
}
TEST(Computation, computation_of_big_real_expression)
{
double res=400000.4*(700.7-5000.5)+400.4;
EXPECT_DOUBLE_EQ(Compute("400000.4*(700.7-5000.5)+400.4"),result);
}
TEST(Computation, not_correct_placement_of_parentheses)
{
ASSERT_ANY_THROW(Compute("(1+2)(/(3+4*6.7)-5.5*4.4"));
}
TEST(Computation, correct_placement_of_parentheses)
{
ASSERT_NO_THROW(Compute("(1+2)/(3+4*6.7)-5.5*4.4"));
}
TEST(Computation, computation_of_little_real_expression)
{
double res=0.4440444*(0.7777055-0.5453345)+0.4345345;
EXPECT_DOUBLE_EQ(Compute("0.4440444*(0.7777055-0.5453345)+0.4345345"),result);
      
