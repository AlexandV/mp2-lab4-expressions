
#include <iostream>
#include <string>
#include <cstdlib>
#include "expression.h"

using namespace std;

int main()
{
  string str;
  cout << "Enter the expression\n" << endl;
  cin >> str;
  cout << endl;
  cout << "Result: " << Calculation(str) << endl;
  system("pause"); 
  return 0;
} 