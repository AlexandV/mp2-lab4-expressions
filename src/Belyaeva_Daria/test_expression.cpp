
#include <gtest.h>
#include "expression.h"

TEST (expression, right_check_brackets)
{
	EXPECT_EQ(3, CheckBrackets("(1+2)*3)"));
}

TEST(expression, right_transfom_sipmle_expression)
{
	EXPECT_EQ("1 2 3*+", ConversionExpression("1+2*3"));
}

TEST(expression, right_transfom_expression_with_brackets)
{
	EXPECT_EQ("1 2+ 3 4+* 5-", ConversionExpression("(1+2)*(3+4)-5"));
}

TEST(expression, can_give_bracket_error_in_transformation)
{
	EXPECT_EQ("error", ConversionExpression("1)-(2*3)-(4+5)"));
}

TEST(expression, right_calculate_simple_expression)
{
	int x = 1 + 2 * 3 - (4 + 5);
	EXPECT_EQ(x, Calculation("1+2*3-(4+5)"));
}

TEST(expression, right_calculate_expression_with_reals)
{
	double x = (1 + 2) / (3 + 4 * 5) - 6 * 7;
	EXPECT_EQ(x, Calculation("(1+2)/(3+4*5)-6*7"));
}