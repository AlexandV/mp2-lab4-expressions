#include <gtest.h>

#include "Expression.h"

TEST(Trans, transformation_simple_expression)
{
	EXPECT_EQ("8 5-", Trans("(8-5)"));
}

TEST(Trans, get_throw_if_not_close_bracket)
{
	ASSERT_ANY_THROW(Trans("(1+1))"));
}

TEST(Trans, get_throw_if_not_close_bracket2)
{
	ASSERT_ANY_THROW(Trans("((1+1)"));
}

TEST(Trans, get_throw_if_have_wrong_symbol)
{
	ASSERT_ANY_THROW(Trans("(a+1)"));
}

TEST(Trans, transformation_with_big_float_number_is_ok)
{
	EXPECT_EQ(Trans("(0.0000024+0.0000318)*100"), "0.0000024 0.0000318+ 100*");
}

TEST(Calc, priority_performed_right)
{
	EXPECT_EQ(8, Calc("(2+2)*2"));
}

TEST(Calc, perform_calculation_with_big_number)
{
	double expectResult = ((21378.347358385 - 53455.0) * 600023 / (2353432.423 + 232));
	EXPECT_EQ(expectResult, Calc("((21378.347358385-53455.0) * 600023/(2353432.423+232))"));
}

TEST(Calc, no_throw_if_all_ok)
{
	ASSERT_NO_THROW(Calc("((1+2)/2)*4"));
}