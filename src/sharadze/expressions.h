#pragma once
#include <iostream>
#include <stack>
#include <string>
using namespace std;

#define ErrorCodeBracket -201
#define ErrorCodeSymbol -202

#define SIZE 256

int SymbolCheck(const string &str);//��������� ������������ ���������� �������
int ErrorCheck(const string &str);//��������� �� ������� ������ �� ��������� ������
int GetPriority(const char &symbol);//���������� ��������� �������� ��� ������
bool IsOperation(const char &symbol);//���������� true, ���� ������ - ��������
string Trans(const string &str);//�������������� ������ �� ��������� ����� � �����������
double Calc(const string &str);//��������� ������

int SymbolCheck(const string &str) //��������� ������������ ���������� �������
{
	int errorCount = 0;
	for (int i = 0; i < int(str.length()); i++)//�������� �� ��������� ������
	{
		if (!isdigit(str[i]) && !IsOperation(str[i]) && !(str[i] == '.') && !(str[i] == '(') && !(str[i] == ')') && !(str[i] == ' '))
		{
			cout << "ERROR! \"" << str[i] << "\" is inadmissible symbol!" << endl;
			errorCount++;
		}
	}
	if (errorCount > 0)
		cout << "Found " << errorCount << " errors" << endl;
	return errorCount;
}

int ErrorCheck(const string &str) //��������� �� ������� ������ �� ��������� ������
{
	stack<int> BracketStack;//���� ��� ������
	int bracketPos[SIZE][2];//��������� ������ ��� �������� �������
	int bracketCount = 0;//���������� ��� �������� �� ������� �������
	int errorCount = 0;//������� ������

	for (int i = 0; i < int(str.length()); i++)//�������� �� ������
	{
		if (str[i] == '(')
			BracketStack.push(i);//����� ����������� ������ ������ � ����
		else if (str[i] == ')')//���������� ����� ����������� ������ � ������� � ���� � ����� ���� �����������
		{					   //���������� �� ����� � ��������������� ������, ���� ���, ���������� � ������ 0 
							   //� ����������� ������� ������
			bracketPos[bracketCount][1] = i;
			if (!BracketStack.empty())
			{

				bracketPos[bracketCount][0] = BracketStack.top();
				BracketStack.pop();
			}
			else
			{
				bracketPos[bracketCount][0] = 0;
				errorCount++;
			}
			bracketCount++;//����������� ������ 
		}
	}

	while (!BracketStack.empty())//��������� �� ������� ����������� ������ (��� ��������������� ��
								 //����������� ������) � �����
	{
		bracketPos[bracketCount][0] = BracketStack.top();
		BracketStack.pop();
		bracketPos[bracketCount][1] = 0;
		bracketCount++;
		errorCount++;
	}


	if (errorCount > 0)//������� ������� ������
	{
		cout << "Found " << errorCount << " errors." << endl;
		cout << "Bracket tab:" << endl;
		cout << "|  Open bracket\t|" << " Close bracket\t|" << endl;
		for (int i = 0; i < bracketCount; i++)
		{
			cout << "|\t" << bracketPos[i][0] << "\t|\t" << bracketPos[i][1] << "\t|" << endl;
		}
	}

	return errorCount;
}

int GetPriority(const char &symbol)//�������� ��������� �������� ��� ������
{
	switch (symbol)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	}
}

bool IsOperation(const char &symbol)//��������� �� ��������
{
	if ((symbol == '+') || (symbol == '-') || (symbol == '*') || (symbol == '/'))
		return true;
	return false;
}

string Trans(const string &str)//������� � ����������� �����
{
	if (SymbolCheck(str))//�������� �� ������� ������ � ��������
	{
		throw ErrorCodeSymbol;
		cout << "Symbol error!" << endl;
		return 0;
	}

	if (ErrorCheck(str))//�������� �� ������� ������ � ����������� ������
	{
		throw ErrorCodeBracket;
		cout << "Bracket error!" << endl;
		return 0;
	}

	stack<char> symbolStack;//���� ��� ��������
	string result;//�������������� ������

	int i = 0;
	while (str[i])
	{
		if (isdigit(str[i]) || str[i] == '.')//�������� �� ����� ��� ����� (��� ������ ������������)
			result += str[i];//����� � ����� ����� ���������� � �������������� ������

		if (IsOperation(str[i]))//�������� �� ��������
		{
			if (symbolStack.empty())
				symbolStack.push(str[i]);
			else
				if (GetPriority(str[i]) > GetPriority(symbolStack.top()))
					symbolStack.push(str[i]);
				//���� ���� ��� �������� ���� ��� ��������� ���������� �������� ���� 
				//���������� ���������� �������� �����, �� �������� ���������� � ����
				else
				{
					while (true)
					{
						result += symbolStack.top();
						symbolStack.pop();
						if (GetPriority(str[i]) > GetPriority(symbolStack.top()) || symbolStack.empty())
							break;
					}
					symbolStack.push(str[i]);
				}
				//���� ��������� ���������� �������� ���� ���������� ���������� �������� � �����,
				//����������� �� ����� �������� � �������������� ������ �� ��� ���, ���� �� �����
				//������������� ������ �������

			result += ' ';//� �������������� ������ ������������ ������ ����� ���������� � �������
		}
		if (str[i] == '(')//����������� ������ ������ �������� � ����
			symbolStack.push(str[i]);
		if (str[i] == ')')
		{
			while (true)
			{
				if (symbolStack.empty())
					break;
				if (symbolStack.top() == '(')
				{
					symbolStack.pop();
					break;
				}
				result += symbolStack.top();
				symbolStack.pop();
			}
		}
		i++;
	}

	while (!symbolStack.empty())
	{
		result += (symbolStack.top());
		symbolStack.pop();
	}
	return result;
}

double Calc(const string &str)
{
	try
	{
		int i = 0;
		int j = 0;
		stack<double> calcStack;
		char number[SIZE] = "";//������ ��� �����

		string resStr = Trans(str);
		while (resStr[i])
		{
			if (isdigit(resStr[i]) || resStr[i] == '.')//�� ��� ��� ���� � ������ �� ��������
				number[j++] += resStr[i];//�� ����� ��� �����, ���������� ����� � ������ ��� �����
			else//� ������ ���������� �� ����� ��� �����
			{
				if (*number != 0)//���� ��������� ������� ���������, ������ � ������� �����
				{//� ������ ����� ��� ��������������� � �������� ��� � ������ � ����
					calcStack.push((double)atof(number));
					memset(number, 0, SIZE*sizeof(char));
					j = 0;
				}

				if (resStr[i] == ' ')//������� ������ ���������
				{
					i++;
					continue;
				}

				double prevNum = 0, temp = 0; //���������� ��� ����������

				prevNum = double(calcStack.top());
				calcStack.pop();
				temp = double(calcStack.top());
				calcStack.pop();

				switch (resStr[i])//��������� �������� ��� ����� ���������� ������� � �����
								  //� ������ � ���� �� ���������
				{
				case '+':
					calcStack.push(temp + prevNum);
					break;
				case '-':
					calcStack.push(temp - prevNum);
					break;
				case '*':
					calcStack.push(temp * prevNum);
					break;
				case '/':
					calcStack.push(temp / prevNum);
					break;
				}
			}
			i++;
		}
		return calcStack.top();
	}
	catch (int error)//��������� ������
	{
		cout << "Error code: ";
		switch (error)
		{
		case ErrorCodeBracket:
			cout << ErrorCodeBracket << endl;
			throw ErrorCodeBracket;
		case ErrorCodeSymbol:
			cout << ErrorCodeSymbol << endl;
			throw ErrorCodeSymbol;
		}
		return 0;
	}
}