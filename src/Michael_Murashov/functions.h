#pragma once

#include <iostream>
#include <string>
#include <stack>

using namespace std;

// �������� ������
bool CtrlBracket(const string &str)
{
	cout << "	Brackets" << endl
		<< "opening		closed" << endl;

	stack<int> st;
	int ErrCount = 0;
	int CountBracket = 0;

	for (int i = 0; i < str.size(); i++)
	{
		if (str[i] == '(')
		{
			st.push(i + 1);
			CountBracket++;
		}

		if (str[i] == ')')
		{
			CountBracket++;

			if (st.empty())
			{
				cout << "   -              " << i + 1 << endl;
				ErrCount++;
			}
			else
			{
				cout << "   " << st.top() << "              " << i + 1 << endl;
				st.pop();
			}
		}// if
	}// for

	if (!st.empty())
	{
		cout << "   " << st.top() << "              -" << endl;
		st.pop();
		ErrCount++;
	}

	if (CountBracket == 0)
		cout << "   -              -" << endl << endl;

	if (ErrCount != 0)
	{
		cout << endl << "You have a " << ErrCount 
			<< " mistakes with brackets!" << endl;
		return false;
	}
	else
	{
		cout << endl;
		return true;
	}
}/*------------------------------------------------------------- */  

// ��������� ��������
int priority(char ch)
{
	switch (ch)
	{
	case '*':
	case '/':
		return 3;

	case '-':
	case '+':
		return 2;

	case '(':
		return 1;

	case ')':
		return 0;
	}
}/*--------------------------------------------------------------*/

// ������� � ������� �������� ������
string transp(const string &str)
{
	string result;
	stack<char> st;
	
	for (int i = 0; i < str.size(); i++)
	{
		if (str[i] == '(')
			st.push(str[i]);

		if (str[i] == '.' || str[i] == ',')
			result += '.';

		if (isdigit(str[i]))
			result += str[i];

		if (str[i] == '*' || str[i] == '/' || str[i] == '+' || str[i] == '-')
		{
			if (st.empty())																			
				st.push(str[i]);
			else
				/* ���� ��������� �������� � ����� ������ ���������� �������� � ������,
				** �������� �������� �� ����� �� ��� ���, ���� �� ����������
				** �������� � ������� ����������� */
				if (priority(str[i]) > priority(st.top()))
					st.push(str[i]);
				else
				{
					while (true)
					{
						result += st.top();
						st.pop();
						if (st.empty() || priority(str[i]) > priority(st.top()))
							break;
					}
					st.push(str[i]);
				}// else

			result += ' ';
		}// if

		/* ���� ����������� ������������� ������, �������� ��� 
		** �������� �� �����, ���� �� ���������� ������������� ������ */
		if (str[i] == ')')
			while (true)
			{
				// ���� ���� ������, ������ �� ������
				if (st.empty())
					break;

				if (st.top() == '(')
				{
					st.pop(); // ������� '('
					break;
				}

				result += st.top();
				st.pop();
			}// while
	}// for

	// ������� � ������ ��� ��������, ���������� � �����
	while (!st.empty())
	{
		result += st.top();
		st.pop();
	}

	return result;
}/*--------------------------------------------------------------*/

// ���������� �� �������� �������� ������
double calc(const string &_str)
{
	stack<double> st;
	char *num; // ������ ��� �������� ������� ����� � ��� double
	int k = 0; // ������� ��� ������ num

	string str = transp(_str);
	num = new char[str.size()];
	memset(num, 0, str.size());

	for (int i = 0; i < str.size(); i++)
	{
		// ���� ������ - ����� ��� �����, ������� ������ � ������ ��� ������� �����
		if (isdigit(str[i]) || str[i] == '.')
			num[k++] = str[i];
		else
		{
			if (k != 0)
			{
				st.push(atof(num));
				// ������� ������
				k = 0;
				memset(num, 0, str.size());
			}
		}
		
		if (str[i] == '*' || str[i] == '/' || str[i] == '+' || str[i] == '-')
		{
			double op1, op2;
			op2 = st.top(); st.pop();
			op1 = st.top(); st.pop();

			switch (str[i])
			{
			case '-': st.push(op1 - op2); break;
			case '+': st.push(op1 + op2); break;
			case '*': st.push(op1 * op2); break;
			case '/': st.push(op1 / op2);
			}
		}// if
	}// for

	if (!st.empty())
		return st.top();
	else
		return atof(num);
}
