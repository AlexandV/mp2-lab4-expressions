#include "functions.h"

int main()
{
	cout << "Hello! This is a cool calculator that can calculate the whole expression!" << endl
		<< "It will take into account the priority of operations." << endl << endl
		<< "You can enter an expression consisting of staples, whole numbers and" << endl
		<< "decimals, multiplication, division, addition and subtraction." << endl;

	string exp;

	while (true)
	{
		while (true)
		{
			cout << endl << "Please, enter an expressions: ";
			cin >> exp;
			cout << endl;

			if (!CtrlBracket(exp))
				cout << "Error!! You are forgot somewhere a bracket!" << endl
					<< "Please, try again and at this time pay attention!" << endl;
			else
			{
				cout << "Errors is not detected!!!" << endl << endl;
				break;
			}// else
		}// while

		cout << "Transp = " << transp(exp) << endl;
		cout << "Result = " << calc(exp) << endl << endl;

		/* ���������� ����������� ��� ������ �� ��������� */
		string _exit;
		while (true)
		{
			cout << "Do you want exit? (y/n): ";
			cin >> _exit;
			if (_exit == "y")
			{
				cout << endl;
				exit(0);
			}
			else
				if (_exit == "n")
					break;
				else
					cout << "O_o WHAT?? Try again!" << endl;
		}
	}// while
}// main