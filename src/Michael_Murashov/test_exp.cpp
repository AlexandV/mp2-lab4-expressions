#include <gtest.h>
#include "functions.h"

TEST(—trlBracket, TEST_1)
{
	EXPECT_EQ(true, CtrlBracket("(1+2)*(3+4)-5"));
}

TEST(transp, TEST_2)
{
	EXPECT_EQ("1 2+ 3 4+* 5-", transp("(1+2)*(3+4)-5"));
}

TEST(—trlBracket, TEST_3)
{
	EXPECT_EQ(false, CtrlBracket("(1+2))*((3+4)-5"));
}

TEST(transp, TEST_4)
{
	EXPECT_EQ("1 2 3*+ 4-", transp("1+2*3-4"));
}

TEST(calc, TEST_5)
{
	EXPECT_EQ(5, calc("5"));
}

TEST(transp, TEST_6)
{
	EXPECT_EQ("123 5+", transp("123+5"));
}

TEST(transp, TEST_7)
{
	EXPECT_EQ("1 0.5+", transp("1+0.5"));
}

TEST(calc, TEST_8)
{
	EXPECT_EQ(16, calc("(1+2)*(3+4)-5"));
}

TEST(calc, TEST_9)
{
	EXPECT_EQ(3, calc("1+2*3-4"));
}

TEST(calc, TEST_10)
{
	EXPECT_EQ(128, calc("123+5"));
}