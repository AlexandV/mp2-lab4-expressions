#include <gtest.h>
#include "expression.h"

TEST(expression, right_transfom_expression_with_reals)
{
	EXPECT_EQ("6 7+ 4 3+* 23-", ConversionExpression("(6+7)*(4+3)-23"));
}

TEST(expression, can_give_bracket_error_in_transformation)
{
	EXPECT_EQ("error", ConversionExpression("7-)(9*6)-(9+3)"));
}
TEST (expression, right_check_brackets)
{
	EXPECT_EQ(2, CheckBrackets("2+2)*2)"));
}

TEST(expression, right_transfom_sipmle_expression)
{
	EXPECT_EQ("2 2 2*+", ConversionExpression("2+2*2"));
}

TEST(expression, right_calculate_simple_expression)
{
	int x = 8+9*6-(1+3);
	EXPECT_EQ(x, Calculation("8+9*6-(1+3)"));
}

TEST(expression, right_calculate_expression_with_reals)
{
	double x = (1+2)/(3+4*7)-3*4;
	EXPECT_EQ(x, Calculation("(1+2)/(3+4*7)-3*4"));
}