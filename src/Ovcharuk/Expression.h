#pragma once
#include <iostream>
#include <stack>
#include <string>
using namespace std;

#define BracketErrorCode -201
#define WrongSymbolCode -202

#define BUFSIZE 256

int SymbolCheck(const string &str);
int ErrorCheck(const string &str);			//�������� �� ������ (������������� � ������������� ������)
int GetPriority(const char &symbol);		//��������� ���������� ��������
bool IsOperation(const char &symbol);		//��������, �������� �� ������� ���������
string Transformation(const string &str);	//�������������� � ����������� ����� �������� ������
double Calculation(const string &str);		//���������� ������ 

int SymbolCheck(const string &str)
{
	int errorCounter = 0;
	for (int i = 0; i < int(str.length()); i++)
	{
		if (!isdigit(str[i]) && !IsOperation(str[i]) && !(str[i] == '.') && !(str[i] == '(') && !(str[i] == ')') && !(str[i] == ' '))					//�������� �� ��, �������� �� ��� ������ ����������
		{
			cout << "ERROR! \"" << str[i] << "\" not is number or operator!" << endl;
			errorCounter++;
		}
	}
	if (errorCounter > 0)
		cout << "Find " << errorCounter << " errors" << endl;
	return errorCounter;
}

int ErrorCheck(const string &str)
{
	stack<int> BracketStack;
	int bracketPosition[BUFSIZE][2];
	int bracketPositionSize = 0;			//������ ������� � �������
	int errorCounter = 0;					//������� ���-�� ��������� ������

	for (int i = 0; i < int(str.length()); i++) //��������� ������ �� ������ 
	{
		if (str[i] == '(')
			BracketStack.push(i);
		else if (str[i] == ')')	//���� ������������� ������
		{
			bracketPosition[bracketPositionSize][1] = i;						//���������� ����� �������������
			if (!BracketStack.empty())											//���� ����� �������������. ���� � ����� �� �����
			{

				bracketPosition[bracketPositionSize][0] = BracketStack.top();	//�� �������� ����� ������������� ������
				BracketStack.pop();
			}
			else																//�����
			{
				bracketPosition[bracketPositionSize][0] = 0;					//������������� �������� 0
				errorCounter++;													//����������� ������� ������
			}
			bracketPositionSize++;
		}
	}

	while (!BracketStack.empty())	//���� ��� �������� �� �������� ������ 
	{
		bracketPosition[bracketPositionSize][0] = BracketStack.top();
		BracketStack.pop();
		bracketPosition[bracketPositionSize][1] = 0;
		bracketPositionSize++;
		errorCounter++;
	}

	//� ���������� ������� ������� �������� � �������� ������ � ���������� ��������� ������ (������ ��� �������, ��� ������ ���� �������)
	if (errorCounter > 0)
	{
		cout << "Find " << errorCounter << " errors." << endl;
		cout << "Bracket tab:" << endl;
		cout << "|  Open bracket\t|" << " Close bracket\t|" << endl;
		for (int i = 0; i < bracketPositionSize; i++)
		{
			cout << "|\t" << bracketPosition[i][0] << "\t|\t" << bracketPosition[i][1] << "\t|" << endl;
		}
	}

	return errorCounter;
}

int GetPriority(const char &symbol)
{
	switch (symbol)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	}
}

bool IsOperation(const char &symbol)
{
	if ((symbol == '+') || (symbol == '-') || (symbol == '*') || (symbol == '/'))
		return true;
	return false;
}

string Transformation(const string &str)
{
	if (SymbolCheck(str)) //��������� �� ������ �������
	{
		throw WrongSymbolCode;
		cout << "Symbol error!" << endl;
		return 0;
	}

	if (ErrorCheck(str)) //��������� �������, ����� �� ������������� ������
	{
		throw BracketErrorCode;
		cout << "Bracket error!" << endl;
		return 0;
	}

	stack<char> operationSymbol;
	string result;

	int i = 0;
	while (str[i])
	{
		if (isdigit(str[i]) || str[i] == '.')	//��������� '.' ��� �������� � ������ ������������� ����
			result += str[i];					//��������� � �������������� ������ ������

		if (IsOperation(str[i]))				//���� ������ ��������
		{
			if (operationSymbol.empty())		//���������, ���� ���� ������
				operationSymbol.push(str[i]);
			else
				if (GetPriority(str[i]) > GetPriority(operationSymbol.top()))	//���� ��������� ����
					operationSymbol.push(str[i]);
				else									//����� ���������� �� ����� ��, ���� �� �������� � ����� ������� �����������
				{
					while (true)
					{
						result += operationSymbol.top();
						operationSymbol.pop();
						if (GetPriority(str[i]) > GetPriority(operationSymbol.top()) || operationSymbol.empty())
							break;
					}
					operationSymbol.push(str[i]);	//��������� ������� ��������
				}

			result += ' ';							//��������� ���������� ����� (���� ��� ������ �� �����)
		}
		if (str[i] == '(')
			operationSymbol.push(str[i]);
		if (str[i] == ')')
		{
			while (true)
			{
				if (operationSymbol.empty())
					break;
				if (operationSymbol.top() == '(')
				{
					operationSymbol.pop();
					break;
				}
				result += operationSymbol.top();
				operationSymbol.pop();
			}
		}
		i++;
	}

	while (!operationSymbol.empty())
	{
		result += (operationSymbol.top());
		operationSymbol.pop();
	}
	return result;
}

double Calculation(const string &str)
{
	try
	{
		int i = 0;
		int j = 0;
		stack<double> calc;
		char number[BUFSIZE] = "";

		string PostfixStr = Transformation(str);
		while (PostfixStr[i])
		{
			if (isdigit(PostfixStr[i]) || PostfixStr[i] == '.') //���� �����, �� ��������� � �����������
				number[j++] += PostfixStr[i];
			else
			{
				if (*number != 0)
				{
					calc.push((double)atof(number));
					memset(number, 0, BUFSIZE*sizeof(char));
					j = 0;
				}

				if (PostfixStr[i] == ' ')
				{
					i++;
					continue;
				}

				//���� ��� ��������� ����� �� ����� � ���������� ���� �� ��������
				double prevNum = 0, temp = 0;

				prevNum = double(calc.top());
				calc.pop();
				temp = double(calc.top());
				calc.pop();

				switch (PostfixStr[i])
				{
				case '+':
					calc.push(temp + prevNum);
					break;
				case '-':
					calc.push(temp - prevNum);
					break;
				case '*':
					calc.push(temp * prevNum);
					break;
				case '/':
					calc.push(temp / prevNum);
					break;
				}
			}
			i++;
		}
		return calc.top();
	}
	catch (int error)
	{
		cout << "Error code: ";
		switch (error)
		{
		case BracketErrorCode:
			cout << BracketErrorCode << endl;
			throw BracketErrorCode;
		case WrongSymbolCode:
			cout << WrongSymbolCode << endl;
			throw WrongSymbolCode;
		}
		return 0;
	}
}