#include "expressions.h"
#include <gtest.h>

TEST(expression, right_transfom_sipmle_expression)
{
	EXPECT_EQ("8 9 6*+ 1 3+-", Transformation("8+9*6-(1+3)"));
}

TEST(expression, right_transfom_expression_with_reals)
{
	EXPECT_EQ("1 2+ 3 4 6.7*+/ 5.3 4.4*-", Transformation("(1+2)/(3+4*6.7)-5.3*4.4"));
}

TEST(expression, can_catch_bracket_throw_in_transformation)
{
	EXPECT_EQ("ERROR", Transformation("8+((9*6)-(1+3)"));
}

TEST(expression, can_catch_transform_throw_in_calculation)
{
	ASSERT_ANY_THROW(Calculation("8+((9*6)-(1+3)"));
}

TEST(expression, right_calculate_simple_expression)
{
	int x = 8 + 9 * 6 - (1 + 3);
	EXPECT_EQ(x, Calculation("8+9*6-(1+3)"));
}

TEST(expression, right_calculate_expression_with_reals)
{
	double x = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	EXPECT_EQ(x, Calculation("(1+2)/(3+4*6.7)-5.3*4.4"));
}