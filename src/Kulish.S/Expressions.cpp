#include "expressions.h"
#include "tsimplestack.h"
#include <cctype>
#include <cstdio>
#include <exception>

using std::cout;
using std::endl;
using std::exception;


int GetPriority(const char &smb)
{
	switch (smb)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	}
}

bool IsOperation(const char &smb)
{
	if ((smb == '+') || (smb == '*') || (smb == '/') || (smb == '-'))
		return true;
	return false;
}

void CollectOnTheStack(TStack<char, MAX> & oper, string &Str)
{
	while (1)
	{
		if (oper.IsEmpty())
			break;
		if (oper.HGet() == '(')
		{
			oper.Get();
			break;
		}
		Str += oper.HGet();
		oper.Get();
	}
}

int BrackesCheck(const string &Str, bool print)
{
	TStack<char, MAX> Control;
	int i = 0;
	int SizeError = 0; 
	int index = 0;
	while (Str[i])
	{
		if (Str[i] == '(')
			Control.Put(++index);
		if (Str[i] == ')')
			if (Control.IsEmpty())
			{

				SizeError++;
				if (print)
					cout << '-' << ' ' << ++index << endl;
				else
					++index;
			}
			else
				if (print)
					cout << (int)Control.Get() << ' ' << ++index << endl;
				else
				{
					Control.Get();
					++index;
				}
		i++;
	}

	if (!Control.IsEmpty())
	{
		SizeError++;
		if (print)
			cout << (int)Control.Get() << ' ' << '-' << endl;
		else
			Control.Get();
	}

	if (print)
		if (SizeError == 0)
			cout << "no errors" << endl;
		else
			cout << "mismatch brackets " << SizeError << endl;
	return SizeError;
}


string Transformation(const string &Str)
{
	try
	{
		if (BrackesCheck(Str, false)) throw  exception("mismatch brackets");
		string result;
		TStack<char, MAX> oper;
		int i = 0;

		while (Str[i])
		{ 
			if (isdigit(Str[i]) || Str[i] == '.')
				result += Str[i];
			if (IsOperation(Str[i]))
			{
				if (oper.IsEmpty())
					oper.Put(Str[i]);
				else
					if (GetPriority(Str[i])>GetPriority(oper.HGet()))
						oper.Put(Str[i]);
					else
					{
						while (1)
						{
							result += oper.Get();
							if (GetPriority(Str[i])>GetPriority(oper.HGet()) || oper.IsEmpty())
								break;
						}
						oper.Put(Str[i]);
					}
				result += ' ';
			}

			if (Str[i] == '(')
				oper.Put(Str[i]);

			if (Str[i] == ')')
				CollectOnTheStack(oper, result);

			i++;
		}

		while (!oper.IsEmpty())
		{
			result += (oper.HGet());
			oper.Get();
		}
		return result;
	}
	catch (exception exc)
	{
		throw exception();
	}
}


double Calculating(const string &Str)
{
	int i = 0, j = 0;
	TStack<float, MAX> calc;
	char number[MAX] = "";
	double temp;


	string PostFix = Transformation(Str);

	while (PostFix[i])
	{
		if (isdigit(PostFix[i]) || PostFix[i] == '.')
			number[j++] += PostFix[i];
		else
		{
			if (*number != 0)
			{
				calc.Put((double)atof(number));
				memset(number, 0, MAX*sizeof(char));
				j = 0;
			}
			if (PostFix[i] == ' ')
			{
				i++;
				continue;
			}
			temp = calc.Get();
			switch (PostFix[i])
			{
			case '+': calc.Put(calc.Get() + temp); break;
			case '-': calc.Put(calc.Get() - temp); break;
			case '*': calc.Put(calc.Get()*temp); break;
			case '/': calc.Put(calc.Get() / temp); break;
			}
		}
		i++;
	}
	return calc.Get();
}