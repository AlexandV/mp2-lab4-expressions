#pragma once
#include <stack>
#include <string>

#define BUFF 256

using namespace std;

	double Calculation(const string &str);
	bool OperationCheck(const char &smb);
	int BrackesCheck(const string &str);
	int GetPriority(const char &smb);
	string Transformation(const string &str);
