#include "gtest.h"

#include "expressions.h"

TEST(Transformation, translation_of_simple_expression)
{
	EXPECT_EQ(Transformation("2*(8-5)+4"), "2 8 5-* 4+");
}

TEST(Transformation, translating_a_simple_expression_of_the_real)
{
	EXPECT_EQ(Transformation("(1+2)/(3+4*6.7)-5.3*4.4"), "1 2+ 3 4 6.7*+/ 5.3 4.4*-");
}

TEST(Transformation, translating_many - digit_number)
{
	EXPECT_EQ(Transformation("100+20000/3500*(1000+21000000)"), "100 20000 3500/ 1000 21000000+*+");
}


TEST(Transformation, translation_expression_with_real_lot_of_decimal_places)
{
	EXPECT_EQ(Transformation("(0.000021+0.00008)*101"), "0.0032 0.0068+ 53*");
}

TEST(Transformation, not_correct_placement_of_parentheses)
{
	ASSERT_ANY_THROW(Transformation("4 * 8 - 3) + 4)"));
}
