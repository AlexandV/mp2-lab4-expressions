#include <iostream>
#include <stack>
#include <locale>

using namespace std;

bool CheckBrackets(char*, bool);
char* GetPostfixForm(char*);
bool IsOperation(char);
int GetOperationPriority(char);
double Calculate(char*);

int main() {
	setlocale(LC_CTYPE, "Russian");	// ����������� ������� ��������

	char buffer[256];
	cin >> buffer;

	if(CheckBrackets(buffer, true))
		cout << "result: " << Calculate(GetPostfixForm(buffer));

	while(true){};

	return 0;
}

double Calculate(char* buffer) {
	char numBuffer[10];
	int currentPos = 0;
	stack<double> st;
	for(int i = 0; buffer[i]; i++) {
		if((buffer[i] == ' ' || IsOperation(buffer[i])) && currentPos > 0) {
			numBuffer[currentPos] = '\0';
			double n = atof(numBuffer);
			st.push(n);
			currentPos = 0;
		}
		if((buffer[i] >= '0' && buffer[i] <= '9') || buffer[i] == '.')
			numBuffer[currentPos++] = buffer[i];
		if(IsOperation(buffer[i])) {
			double n1 = st.top();
			st.pop();
			double n2 = st.top();
			st.pop();
			double r = 0;
			switch(buffer[i]) {
			case '+':
				r = n1 + n2;
				break;
			case '-':
				r = n1 - n2;
				break;
			case '*':
				r = n1 * n2;
				break;
			case '/':
				r = n1 / n2;
				break;
			}
			st.push(r);
		}
	}
	return st.top();
}

char* GetPostfixForm(char* buffer) {
	char* result = new char[256];
	int currentPos = 0;
	stack<char> st;
	for(int i = 0; buffer[i]; i++) {
		char c = buffer[i];
		if(IsOperation(c)) {
			int p = GetOperationPriority(c);
			if(p == 0 || st.empty()) {
				st.push(c);
				result[currentPos++] = ' ';
				continue;
			}
			if(c == ')') {
				while(st.top() != '(') {
					result[currentPos++] = st.top();
					st.pop();
				}
				st.pop();
				continue;
			}
			if(!st.empty() && p > GetOperationPriority(st.top())) {
				st.push(c);
				result[currentPos++] = ' ';
				continue;
			}
			if(!st.empty() && p <= GetOperationPriority(st.top())) {
				while(!st.empty() && GetOperationPriority(st.top()) >= p) {
					result[currentPos++] = st.top();
					st.pop();
				}
				st.push(c);
				result[currentPos++] = ' ';
			}
		} else 
			result[currentPos++] = c;
	}
	while(!st.empty())
		if(st.top() != '(' && st.top() != ')') {
			result[currentPos++] = st.top();
			st.pop();
		}
	result[currentPos] = '\0';
	return result;
}

bool IsOperation(char c) {
	char* operations = "+-*/()";
	for(int i = 0; operations[i]; i++)
		if(operations[i] == c)
			return true;
	return false;
}

int GetOperationPriority(char c) {
	switch(c) {
	case '(':
		return 0;
	case ')':
		return 1;
	case '+':
	case '-':
		return 2;
	case '*':
	case '/':
		return 3;
	default:
		return -1;
	}
}

bool CheckBrackets(char* buffer, bool print) {
	bool isValid = true;	// ������ ������

	stack<int> st;	// ����

	int count = 0;	// ���������� ����� ������� ������

	if(print)
		cout << "� �" << endl;

	for(int i = 0; buffer[i]; i++) {
		if(buffer[i] == '(')
			st.push(++count);
		if(buffer[i] == ')') {
			count++;
			if(st.empty()) {
				isValid = false;
				if(print)
					cout << 0 << " " << count << endl;
			}
			else {
				if(print)
					cout << st.top() << " " << count << endl;
				st.pop();
			}
		}
	}

	while(!st.empty()) {
		isValid = false;
		if(print)
			cout << st.top() << " " << 0 << endl;
		st.pop();
	}

	if(!isValid && print)
		cout << "��������� �������� ������" << endl;

	return isValid;
}