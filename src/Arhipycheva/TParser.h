#include "TStack.h"

class TParser
{

private:
	char Inf[MaxLen];
	char Post[MaxLen];
	char Orig[MaxLen];
	TStack<char> st_c;
	TStack<double> st_d;

public:
	TParser(char *str = NULL);
	bool Check_of_brackets();
	double Polish_system();
	void InfToPost();
	void Clear();
	char* Get_Inf() { return Inf; SetToOrig(Inf); };
	char* Get_Post() { return Post; };
	char* Get_Orig() { return Orig; };
	void SetToInf(char *str);
	void SetToOrig(char *str);

	~TParser();

private:
	void Check_of_brackets(int &pos, char *str);
	int Priority(char symb);
	bool IsOpearation(char symb);
	void Normalization();
	void Inf_Negative();
	void Add_Zero();

};

char* FuncToExpr(char* str_f, char symb_f, int x_f);