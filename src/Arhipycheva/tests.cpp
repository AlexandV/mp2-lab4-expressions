#include <gtest.h>
#include "TParser.h"

TEST(expression, right_check_brackets)
{
	EXPECT_EQ(false, Check_of_brackets("5+6)*7)"));
}

TEST(expression, right_transfom_sipmle_expression)
 {
	EXPECT_EQ("2 2 2*+", InfToPost("2+2*2"));
 }

TEST(expression, right_transfom_expression)
 {
	EXPECT_EQ("6 7+ 4 3+* 23-", InfToPost("(6+7)*(4+3)-23"));
 }

TEST(expression, right_calculate_simple_expression)
 {
	Add a comment to this line
	 int x = 10 + 5 * 6 - (2 + 3);
	EXPECT_EQ(x, Polish_system("10+5*6-(2+3)"));
}

TEST(expression, right_calculate_expression_with_reals)
Add a comment to this line
 {
	double x = (1 + 2) / (3 + 4 * 7) - 3 * 4;
	EXPECT_EQ(x, Polish_system("(1+2)/(3+4*7)-3*4"));
 }