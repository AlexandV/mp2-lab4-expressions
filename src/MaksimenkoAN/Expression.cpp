#include<C:\Users\Jarvi\Desktop\vc\experession\experession\Expression.h>

int —ontrolBrackets(const string &s)
{
	stack<int> st;
	int countbrackets = 0;
	int CountError = 0;
	int index = 0;

	for (int i = 0; i < s.size(); i++)
	{
		if (s[i] == '(')
		{
			st.push(++index);
			countbrackets++;
		}
		if (s[i] == ')')
		{
			countbrackets++;
			if (st.empty())
			{
				CountError++;
				cout << " -  " << ++index << endl;
			}
			else
			{
				cout << " " << st.top() << "  " << ++index << endl;
				st.pop();
			}
		}
	}
	if (!st.empty())
	{
		cout << " " << st.top() << "  -" << endl;
		st.pop();
		CountError++;
	}
	if (countbrackets == 0)
	{
		cout << " -  -" << endl;
		cout << endl;
	}
	if (CountError != 0)
	{
		cout << CountError << "Error's with bracket's" << endl;
		return CountError;
	}
	else
	{
		cout << endl;
		return CountError;
	}
}

int priority(const char c)
{
	switch (c)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	}
}

string transformer(const string &s)
{
	string result;
	stack<char> st;
	int count = 0;
	for (int i = 0; i < s.size(); i++)
	{
		if (s[i] == '(')
		{
			st.push(s[i]);
		}
		if (s[i] == '.')
			result += '.';
		if (isdigit(s[i]))
		{
			count = 1;
			result += s[i];
		}
		if ((s[i] == '+') || (s[i] == '-') || (s[i] == '*') || (s[i] == '/'))
		{
			if (st.empty())
				st.push(s[i]);
			else
				if (priority(s[i])>priority(st.top()))
					st.push(s[i]);
				else
				{
					while (true)
					{
						result += st.top();
						st.pop();
						if (st.empty() || priority(s[i]) > priority(st.top()))
							break;
					}
					st.push(s[i]);
				}
			result += ' ';
		}
		if (s[i] == ')')
			while (true)
			{
				if (st.empty())
					break;
				if (st.top() == '(')
				{
					st.pop();
					break;
				}
				result += st.top();
				st.pop();
			}
	}
	while (!st.empty())
	{
		result += st.top();
		st.pop();
	}
	return result;
}

double Compute(const string &s)
{
	string postfix = transformer(s);
	int len = postfix.length();

	stack<double> st;
	char numer[255] = { 0 };
	int count = 0;

	for (int i = 0; i < postfix.length(); i++)
	{
		if ((isdigit(postfix[i])) || (postfix[i] == '.'))
			numer[count++] = postfix[i];
		else
			if (count != 0)
			{
				st.push(atof(numer));
				count = 0;
				memset(numer, 0, 255);
			}
		if ((postfix[i] == '*') || (postfix[i] == '/') || (postfix[i] == '+') || (postfix[i] == '-'))
		{
			double o1, o2;
			o1 = st.top();
			st.pop();
			o2 = st.top();
			st.pop();
			switch (postfix[i])
			{
			case '+': st.push(o2 + o1); break;
			case '-': st.push(o2 - o1); break;
			case '*': st.push(o2 * o1); break;
			case '/': st.push(o2 / o1);
			}
		}
	}
	return st.top();
}
